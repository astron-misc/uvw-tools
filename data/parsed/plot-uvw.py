#!/bin/env python

# imports
import os
import sys
import numpy as np
import matplotlib.pyplot as plt

# read command line arguments
argv = sys.argv
argc = len(argv)
if argc != 2:
    print(("Usage:\n\t%s file" %(argv[0])))
    exit()
filename = argv[1]

# read data
script_dir=os.path.dirname(os.path.realpath(__file__))
data=open(f"{script_dir}/{filename}").read().split()

# split data in u, v and w
interval = 1
u=np.asarray([float(e.split(",")[0]) for e in data[::interval]])
v=np.asarray([float(e.split(",")[1]) for e in data[::interval]])
w=np.asarray([float(e.split(",")[2]) for e in data[::interval]])

# open figure
fig = plt.figure()
ax = fig.add_subplot(1,1,1)

# add ticks to figure
#major_ticks = np.arange(-max(abs(u)), max(abs(u)), 64)
#minor_ticks = np.arange(-max(abs(v)), max(abs(v)), 16)
#ax.set_xticks(major_ticks)
#ax.set_xticks(minor_ticks, minor=True)
#ax.set_yticks(major_ticks)
#ax.set_yticks(minor_ticks, minor=True)

# add grid to figure
ax.grid(which='both')

# disable axis labels
#ax.xaxis.set_ticklabels([])
#ax.yaxis.set_ticklabels([])

# make figure square
ax.set_aspect(1)

# plot data
plt.plot(u,v,"ko", ms=1)

# show figure
plt.show()
