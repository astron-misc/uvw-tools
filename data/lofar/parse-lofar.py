#!/bin/env python3

# imports
import os
import sys
import numpy as np
import matplotlib.pyplot as plt

# read command line arguments
argv = sys.argv
argc = len(argv)
if argc != 2:
    print(("Usage:\n\t%s type" %(argv[0])))
    exit()
antenna_type = argv[1]

# read data (station coordinates)
script_dir=os.path.dirname(os.path.realpath(__file__))
data=open(f"{script_dir}/etrs-phase-centres.csv").read().split()

# remove header
data = data[1:]

# create list of station coordinates
station_u = list()
station_v = list()
station_w = list()
for line in data:
    s = line.split(",")
    name = s[0]
    type = s[1]
    if (type == antenna_type):
        u = float(s[2])
        v = float(s[3])
        w = float(s[4])
        station_u.append(u)
        station_v.append(v)
        station_w.append(w)

# read data (antenna coordinates)
data=open(f"{script_dir}/etrs-antenna-positions.csv").read().split()

# remove header
data = data[1:]

# create list of antenna coordinates
antenna_u = list()
antenna_v = list()
antenna_w = list()
for line in data:
    s = line.split(",")
    name = s[0]
    type = s[1]
    if (type == antenna_type):
        u = float(s[3])
        v = float(s[4])
        w = float(s[5])
        antenna_u.append(u)
        antenna_v.append(v)
        antenna_w.append(w)

# open figure
fig = plt.figure()
ax = fig.add_subplot(1,1,1)

# add grid to figure
ax.grid(which='both')

# make figure square
ax.set_aspect(1)

# plot data
plt.plot(antenna_u, antenna_v, "ko", ms=1)
plt.plot(station_u, station_v, "ro", ms=3)

# show figure
plt.show()
