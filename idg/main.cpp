#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <omp.h>

#include "Globals.h"
#include "init.h"

using namespace std;


int main(int argc, char **argv)
{
	// Info
	cout << "Configuration:" << endl;
    cout << "NR_TIMESTEPS = " << NR_TIMESTEPS << endl;
    cout << "NR_POLARIZATIONS = " << NR_POLARIZATIONS << endl;
    cout << "NR_STATIONS = " << NR_STATIONS << endl;
    cout << "NR_BASELINES = " << NR_BASELINES << endl;
    cout << "INTEGRATION_TIME = " << INTEGRATION_TIME << endl;

	// Size
    size_t size_uvw = 1ULL * NR_BASELINES * NR_TIMESTEPS * sizeof(UVW);

    // Data
	cout << "Allocate data" << endl;
    UVWType *uvw = (UVWType *) malloc(size_uvw);

    // Initialize data
	cout << "Initialize data" << endl;
	init_uvw(uvw, NR_STATIONS, NR_BASELINES, NR_TIMESTEPS, INTEGRATION_TIME);

	// Output uvw to file
	std::cout << "Output UVW to file" << std::endl;
	print_uvw(uvw, NR_BASELINES, NR_TIMESTEPS);

    // Free memory
    free(uvw);

    return EXIT_SUCCESS;
}
