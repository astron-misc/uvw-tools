#pragma once

#include <complex>
#include <cstring>

#include "Globals.h"
#include "uvwsim.h"

void print_uvw(void *ptr, int nr_baselines, int nr_time);

void init_uvw(void *ptr, int nr_stations, int nr_baselines,
              int nr_time, int integration_time);
