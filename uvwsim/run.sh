#!/bin/sh

DATA_DIR=../data/parsed
TEMP_FILE=uvw_$$
export LAYOUT_FILE=${DATA_DIR}/SKA1_low.txt
make generate-uvw
./generate-uvw > ${DATA_DIR}/${TEMP_FILE}
${DATA_DIR}/plot-uvw.py ${TEMP_FILE}
rm ${DATA_DIR}/${TEMP_FILE}
